/**
 * Created by ���� on 12.08.2015.
 */
//Node modules
var express = require('express');
var app = express();
var router  = express.Router();
var path = require('path');
var cookies = require('cookies');
var multer = require('multer');
var passport = require('passport');

//Own requiers
var reqHandler = require ('./requestHandler.js');

// Code
var root = path.join(__dirname, "./../");

/*router.post('/login', passport.authenticate('login', {
 successRedirect: '/mainPage',
 failureRedirect: '/SignInPage',
 failureFlash : true
 }));*/

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './front/pictures/upload/')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + ".jpg")
    }
});

router.post('/upload', multer( {
 dest: './front/pictures/upload/',
 limits: {fileSize: 5000000, files:1},
 storage : storage
 }).single('image'), function(req,res, next){
 //console.log(req.body); //form fields
 //console.log(req.file); //form files
 reqHandler.addPicture(
 req.file.fieldname,
 req.file.originalname,
 req.file.encoding,
 req.file.mimetype,
 req.file.destination,
 req.file.filename,
 req.file.path,
 req.file.size, res, next);
 });

router.get('/registerPage', function(req, res){
    res.sendFile(path.join(root, "front/index.html"));
});

router.get('/homePage', function(req, res){
    res.sendFile(path.join(root, "front/homePage.html"));

});

router.get('/logOut', function(req, res){
    res.redirect("http://localhost:3000/homePage");
});

router.get('/mainPage', function(req, res){

    if (req.cookies.isLogIn == 1){
        reqHandler.returnStatus(req.cookies.user, res);
    }
        /*console.log("COOKIE - " + req.cookies.isLogIn);
        console.log("status - " + reqHandler.returnStatus(req.cookies.user));
        if (reqHandler.returnStatus(req.cookies.user) == 3) {
            reqHandler.crateAdminPage(res);
        }
        else {

            reqHandler.createUserPage(res);
            console.log("fuck u!!!!!!!!!!!!!")
        }
    }*/
    else{
        res.redirect("http://localhost:3000/homePage");
    }
});

router.get('/', function(req, res){
    if (req.cookies.isLogIn == 1){
        res.redirect("http://localhost:3000/mainPage");
    }
    else{
        res.redirect("http://localhost:3000/homePage");
    }
});

router.get('/SignInPage', function(req, res){
    res.sendFile(path.join(root, "front/SignInPage.html"));
});

router.post('/signIn', function(req, res, next){
    reqHandler.signIn(req, res, next);
});

router.post('/register', function(req, res, next){
    reqHandler.register(req, res, next);
});

router.get('/apply', function (req, res, next){
    console.log("apply route");
    reqHandler.chekUser(req, res, next);
});

router.get('/goToRegisterPage', function (req, res) {
    reqHandler.goToRegisterPage(res);
});

router.get('/goToSignInPage', function (req, res) {
    reqHandler.goToSignInPage(res);
});

router.get('/deleteUsers', function(req, res){
    reqHandler.deleteUsers(res);
});

exports.router = router;