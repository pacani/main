var express     = require('express');
var app         = express();
var http        = require('http');
var bodyParser  = require('body-parser');
var url         = require("url");
var router      = require('./../router/router.js');
var cookies = require('cookies');
var cookieParser = require('cookie-parser');
/*var passport = require('passport');

app.use(passport.initialize());
app.use(passport.session());*/

app.set('port', 3000);
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(router.router);
app.use(express.static('./front',{index:false, mainPage: false, login: false, homePage : false}));

app.use(function(err, req, res, next) {
    //if(res.status(404)) {console.log(err);}
    res.send(err);
    //next();
});

function startServer () {
    http.createServer(app).listen(app.get('port'), function () {
        console.log('server is started');
    });
}

exports.startServer = startServer;