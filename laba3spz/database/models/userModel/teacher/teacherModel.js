var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var teacherSchema = mongoose.Schema({
    firstName : String,
    lastName : String,
    eMail : String,
    phoneNumber : String,
    password : String,
    userStatus : Number,
    homeworkLinks : {type : [Object], default : []},
    isClassroomTeacher : Boolean,
    subject : Object
});

teacherSchema.statics.createUser = function (user,  res, next) {
    teachers.findOne({userName: user.userName}, function (err, docs) {
        if (docs != null) {
            next({error: true, msg: 'error, this userName already used'});
        }
        else {
            teachers.findOne({eMail: user.eMail}, function (err, docs) {
                if (docs != null) {
                    next({error: true, msg: 'error, this email already used'});
                }
                else {
                    var eMailRegular = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
                    var userNameRegular = /^[-a-z0-9~!$%^&*_=+}{\'?]+/i;
                    if (!eMailRegular.test(user.eMail)) {
                        next({error: true, msg: "incorrect eMail"});
                    }
                    else if (!userNameRegular.test(user.userName)) {
                        next({error: true, msg: "incorrect userName"});
                    }
                    else if (user.password.length < 10) {
                        next({error: true, msg: "password must be 10+ symbols"});
                    }
                    else {
                        var newUser = new teachers({
                            firstName : user.firstName,
                            lastName : user.lastName,
                            eMail : user.eMail,
                            phoneNumber : user.phoneNumber,
                            password : user.password,
                            userStatus : user.userStatus,
                            homeworkLinks : user.homeworkLinks,
                            isClassroomTeacher : user.isClassroomTeacher,
                            subject : user.subject
                        });
                        newUser.save(function (err) {
                            if (err) next (err);
                            else{
                                console.log('CRATE USER--------');
                                console.log(newUser);
                            }
                        });
                    }
                }
            });
        }
    });
};

teacherSchema.statics.editUser = function (user,  res, next) {
    teachers.findOne({eMail: user.eMail}, function (err, docs) {
        if (docs != null) {

            docs.firstName = user.firstName;
            docs.lastName = user.lastName;
            docs.eMail = user.eMail;
            docs.phoneNumber = user.phoneNumber;
            docs.password = user.password;
            docs.userStatus = user.userStatus;
            docs.homeworkLinks = user.homeworkLinks;
            docs.isClassroomTeacher = user.isClassroomTeacher;
            docs.subject = user.subject;

            docs.save(function (err) {
                "use strict";
                if (err) next (err);
                else console.log ("Edited User ---------------- " + user.firstName)
            })
        }
        else {
            next({error: true, msg: 'db does not consist this user'});
        }
    });
};

var teachers = mongoose.model('teachers', teacherSchema);

exports.teachers = teachers;