var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var teachersModel = require ('../teacher/teacherModel.js');
var teachers = teachersModel.teachers;

var studentsModel = require ('../student/studentModel.js');
var students = studentsModel.teachers;

var lessonsModel = require ('../../lessonModel/lessonModel.js');
var lessons = lessonsModel.teachers;

var adminSchema = mongoose.Schema({
    firstName : String,
    lastName : String,
    eMail : String,
    phoneNumber : String,
    password : String,
    userStatus : Number
});

//studentStatus - 1
//teacher - 3
//admin - 4

adminSchema.statics.createUser = function (user,  res, next) {
    switch (user.status) {
        case "1" : students.createUser(user,  res, next);
            break;
        case "3" : teachers.createUser(user,  res, next);
            break;
    }
};


adminSchema.statics.editUser = function (user,  res, next) {
    switch (user.status) {
        case "1" : students.editUser(user,  res, next);
            break;
        case "3" : teachers.editUser(user,  res, next);
            break;
    }
};

adminSchema.statics.createLesson = function (lesson,  res, next) {
    lessons.createLesson(lesson,  res, next);
};

var admins = mongoose.model('admins', adminSchema);

exports.admins = admins;