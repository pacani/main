/**
 * Created by ���� on 11.05.2016.
 */
/**
 * Created by Pusy penetrator 3000 on 12.08.2015.
 */
var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var homeworkSchema = mongoose.Schema({
    homeworkStatus : [String],
    description : String,
    materials : Object,
    teacher : Object,
    subject : Object,
    class : Object,
    marks : {type : [Number], default : []},
    answers : {type : [Object], default : []},
    comments : {type : [[String]], default : [[]]}
});

var homeworks = mongoose.model('homeworks', homeworkSchema);

exports.homeworks = homeworks;

