/**
 * Created by ���� on 11.05.2016.
 */
/**
 * Created by Pusy penetrator 3000 on 12.08.2015.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var lessonSchema = mongoose.Schema({
    subject: Object,
    classroomNumber: Number,
    lessonNumber: Number,
    homeworkLink: String,
    description: String,
    teacher: Object,
    materials: [Object],
    date: {type: Date, default: Date.now()}
});

lessonSchema.statics.createLesson = function (lesson, res, next) {
    lessons.findOne({date: lesson.date}, function (err, docs) {
        if (docs != null) {
            next({error: true, msg: 'error, this time already used'});
        }
        else {
            var newLesson = new lessons({

                subject: lesson.subject,
                classroomNumber: lesson.classroomNumber,
                lessonNumber: lesson.lessonNumber,
                homeworkLink: lesson.homeworkLink,
                description: lesson.description,
                teacher: lesson.teacher,
                materials: lesson.materials,
                date: lesson.date

            });
            newLesson.save(function (err) {
                if (err) next(err);
                else {
                    console.log('CRATED LESSON--------');
                    console.log(newLesson);
                }
            });
        }
    });
};

var lessons = mongoose.model('lessons', lessonSchema);

exports.lessons = lessons;

