var mongoose = require('mongoose');
var config = require("./../config.js");
var db = config.dbConfig;

var dbConnect = function () {
    mongoose.connect("mongodb://" + db.dbLogin + ":" + db.dbPassword + db.dbUrl);
    mongoose.connection.on("open", function () {
        console.log("connection to database done!");
    });
    mongoose.connection.on("error", function() {
        console.log("error");
    });
};

exports.dbConnect = dbConnect;
